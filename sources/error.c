#include <SDL2/SDL.h>

#include "error.h"

void
error_print(const char *location, const char *text) {
    printf("\nError at %s: '%s' "
           "\n> %s\n", 
           location, 
           text, 
           SDL_GetError());
}

#include <math.h>

#include "game.h"
#include "board.h"
#include "cell.h"
#include "mouse.h"
#include "rect.h"
#include "renderer.h"
#include "rgba32.h"
#include "texture.h"

static void 
load(game_t *game) {
    board_create(game->renderer, &game->board);
}

static void 
update(board_t *board, event_t *event) {
    if (event->type != SDL_MOUSEBUTTONDOWN)
        return;

    int mx, my;
    mouse_get_state(&mx, &my);

    board_set_current_cell(board, floor(mx / CELL_SIZE), floor(my / CELL_SIZE));
    board_move_to_cell(board, floor(mx / CELL_SIZE), floor(my / CELL_SIZE));
}

static void 
render(game_t *game) {
    board_render(&game->board, game->renderer);
}

game_t*
game_create() {
    game_t *g = malloc(sizeof(game_t));

    SDL_Init(SDL_INIT_VIDEO | SDL_INIT_EVENTS);
    IMG_Init(IMG_INIT_PNG);

    window_params_t params = WINDOW_PARAMS_DEFAULT;
    params.title = "Chess";
    params.w = BOARD_SIZE * CELL_SIZE;
    params.h = BOARD_SIZE * CELL_SIZE;

    g->window = window_create(&params); 
    g->renderer = renderer_create(g->window);

    load(g);
    while (1) {
        event_t e;
        if (event_poll(&e)) {
            if (e.type == SDL_QUIT)
                break;

            update(&g->board, &e);
        }

        render(g);
        renderer_update(g->renderer);
    }

    return g;
}

void 
game_destroy(game_t *g) {
    board_destroy(&g->board);
    renderer_destroy(g->renderer);
    window_destroy(g->window);

    IMG_Quit();
    SDL_Quit();

    free(g);
}

#include <SDL2/SDL_image.h>

#include "surface.h"
#include "error.h"

surface_t*
surface_load(const char *path) {
    surface_t *surface = IMG_Load(path);

    if (surface == NULL)
        error_print(AT, "The surface is NULL");

    return surface;
}

void 
surface_destroy(surface_t *surface) {
    SDL_FreeSurface(surface);
}

#include "game.h"

int 
main() {
    game_t *chess = game_create();

    game_destroy(chess);
    return 0;
}

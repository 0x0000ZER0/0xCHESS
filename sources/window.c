#include "window.h"
#include "error.h"

window_t*
window_create(window_params_t *params) {
     window_t *win = SDL_CreateWindow(params->title, 
                                      params->x, 
                                      params->y,
                                      params->w, 
                                      params->h, 
                                      SDL_WINDOW_SHOWN);
    if (win == NULL)
        error_print(AT, "The window is NULL");

    return win;
}

void 
window_destroy(window_t *win) {
    SDL_DestroyWindow(win);
}

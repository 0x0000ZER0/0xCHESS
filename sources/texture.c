#include <SDL2/SDL_image.h>
#include "texture.h"
#include "error.h"

texture_t*
texture_load(renderer_t *renderer, const char *path) {
    texture_t *tex = IMG_LoadTexture(renderer, path);

    if (tex == NULL)
        error_print(AT, "The texture is NULL");

    return tex;
}

void 
texture_destroy(texture_t *texture) {
    SDL_DestroyTexture(texture);
}

void
texture_set_color(texture_t* tex, rgba32_t* color) {
    SDL_SetTextureColorMod(tex, color->red, color->green, color->blue);
}

texture_t*
texture_create_from_surface(renderer_t *renderer, surface_t *surface) {
    texture_t *tex = SDL_CreateTextureFromSurface(renderer, surface);

    if (tex == NULL)
        error_print(AT, "The texture is NULL");

    return tex;
}

#include "piece.h"
#include "rgba32.h"
#include "texture.h"

piece_t*
piece_create() {
    piece_t *piece = malloc(sizeof(piece_t));
    piece->type    = PIECE_TYPE_NONE;
    piece->color   = PIECE_COLOR_NONE;
    piece->texture = NULL;

    return piece;
}

piece_t*
piece_create_pawn(renderer_t *ren, char color) {
    piece_t *piece = malloc(sizeof(piece_t));
    piece->type    = PIECE_TYPE_PAWN;
    piece->color   = color;

    const char *path = piece->color != PIECE_COLOR_BLACK ? "pawn-white.png" : "pawn-black.png";
                        

    piece->texture = texture_load(ren, path);

    return piece;
}

piece_t*
piece_create_rook(renderer_t *ren, char color) {
    piece_t *piece = malloc(sizeof(piece_t));
    piece->type    = PIECE_TYPE_ROOK;
    piece->color   = color;

    const char *path = piece->color != PIECE_COLOR_BLACK ? "rook-white.png" : "rook-black.png";
                        

    piece->texture = texture_load(ren, path);

    return piece;
}

piece_t*
piece_create_knight(renderer_t *ren, char color) {
    piece_t *piece = malloc(sizeof(piece_t));
    piece->type    = PIECE_TYPE_KNIGHT;
    piece->color   = color;

    const char *path = piece->color != PIECE_COLOR_BLACK ? "knight-white.png" : "knight-black.png";
                        

    piece->texture = texture_load(ren, path);

    return piece;
}
piece_t*
piece_create_bishop(renderer_t *ren, char color) {
    piece_t *piece = malloc(sizeof(piece_t));
    piece->type    = PIECE_TYPE_BISHOP;
    piece->color   = color;

    const char *path = piece->color != PIECE_COLOR_BLACK ? "bishop-white.png" : "bishop-black.png";
                        

    piece->texture = texture_load(ren, path);

    return piece;
}
piece_t*
piece_create_queen(renderer_t *ren, char color) {
    piece_t *piece = malloc(sizeof(piece_t));
    piece->type    = PIECE_TYPE_QUEEN;
    piece->color   = color;

    const char *path = piece->color != PIECE_COLOR_BLACK ? "queen-white.png" : "queen-black.png";
                        

    piece->texture = texture_load(ren, path);

    return piece;
}

piece_t*
piece_create_king(renderer_t *ren, char color) {
    piece_t *piece = malloc(sizeof(piece_t));
    piece->type    = PIECE_TYPE_KING;
    piece->color   = color;

    const char *path = piece->color != PIECE_COLOR_BLACK ? "king-white.png" : "king-black.png";
                        

    piece->texture = texture_load(ren, path);

    return piece;
}

void 
piece_destroy(piece_t *piece) {
    if (piece == NULL)
        return;

    texture_destroy(piece->texture);

    free(piece);
}

void 
piece_render(piece_t *piece, renderer_t *ren, rect_t *rec) {
    rgba32_t color = { 200, 200, 200, 0 };

    texture_set_color(piece->texture, &color);
    renderer_draw_texture(ren, piece->texture, rec);     
}

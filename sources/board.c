#include "board.h"
#include "cell.h"
#include "piece.h"
#include "renderer.h"
#include "rgba32.h"
#include "texture.h"


static void
board_make_default_cell(cell_t *cell) {
    unsigned char ctyp = (cell->row + cell->col) % 2 == 0 ? 255 : 0;

    cell->color.red   = ctyp;
    cell->color.green = ctyp;
    cell->color.blue  = ctyp;
}

static void 
board_make_blue_cell(cell_t *cell) {
    cell->color.red   = 0;
    cell->color.green = 0;
    cell->color.blue  = 255;
}

static void 
board_make_red_cell(cell_t *cell) {
    cell->color.red   = 255;
    cell->color.green = 0;
    cell->color.blue  = 0;
}

static void 
board_make_green_cell(cell_t *cell) {
    cell->color.red   = 0;
    cell->color.green = 255;
    cell->color.blue  = 0;
}

static void
board_setup(board_t *brd, renderer_t *ren) {
    //=================== BLACK =======================
    brd->cells[0][0].piece = piece_create_rook(ren, PIECE_COLOR_BLACK);
    brd->cells[7][0].piece = piece_create_rook(ren, PIECE_COLOR_BLACK);

    brd->cells[1][0].piece = piece_create_knight(ren, PIECE_COLOR_BLACK);
    brd->cells[6][0].piece = piece_create_knight(ren, PIECE_COLOR_BLACK);

    brd->cells[2][0].piece = piece_create_bishop(ren, PIECE_COLOR_BLACK);
    brd->cells[5][0].piece = piece_create_bishop(ren, PIECE_COLOR_BLACK);
    
    brd->cells[3][0].piece = piece_create_queen(ren, PIECE_COLOR_BLACK);
    
    brd->cells[4][0].piece = piece_create_king(ren, PIECE_COLOR_BLACK);
    
    for (int i = 0; i < BOARD_SIZE; ++i) 
        brd->cells[i][1].piece = piece_create_pawn(ren, PIECE_COLOR_BLACK);
    
    //=================== WHITE =======================
    brd->cells[0][7].piece = piece_create_rook(ren, PIECE_COLOR_WHITE);
    brd->cells[7][7].piece = piece_create_rook(ren, PIECE_COLOR_WHITE);

    brd->cells[1][7].piece = piece_create_knight(ren, PIECE_COLOR_WHITE);
    brd->cells[6][7].piece = piece_create_knight(ren, PIECE_COLOR_WHITE);

    brd->cells[2][7].piece = piece_create_bishop(ren, PIECE_COLOR_WHITE);
    brd->cells[5][7].piece = piece_create_bishop(ren, PIECE_COLOR_WHITE);
    
    brd->cells[3][7].piece = piece_create_queen(ren, PIECE_COLOR_WHITE);
    
    brd->cells[4][7].piece = piece_create_king(ren, PIECE_COLOR_WHITE);
    
    for (int i = 0; i < BOARD_SIZE; ++i) 
        brd->cells[i][6].piece = piece_create_pawn(ren, PIECE_COLOR_WHITE);
}

void 
board_create(renderer_t *ren, board_t *brd) {
    brd->affected_cells_size = 0;

    for (int r = 0; r < BOARD_SIZE; ++r)
        for (int c = 0; c < BOARD_SIZE; ++c) {
            brd->cells[r][c].col = c;
            brd->cells[r][c].row = r;
           
            board_make_default_cell(&brd->cells[r][c]);

            brd->cells[r][c].piece = NULL;
        }
    
    brd->current_cell = NULL;
    
    board_setup(brd, ren);
}

void
board_destroy(board_t *brd) {
    for (int r = 0; r < BOARD_SIZE; ++r)
        for (int c = 0; c < BOARD_SIZE; ++c) 
            piece_destroy(brd->cells[c][r].piece);
}

void 
board_render(board_t *brd, renderer_t *ren) {
    for (int r = 0; r < BOARD_SIZE; ++r)
        for (int c = 0; c < BOARD_SIZE; ++c) {
            cell_render(&brd->cells[r][c], ren);

            if (brd->cells[r][c].piece != NULL) {
                rect_t rect = { 
                    r * CELL_SIZE, 
                    c * CELL_SIZE,  
                    CELL_SIZE,
                    CELL_SIZE
                };
            
                piece_render(brd->cells[r][c].piece, ren, &rect);
            }            
        }
}

static void 
board_clear_affected_cells(board_t *board) {
    for (int i = 0; i < board->affected_cells_size; ++i) 
        board_make_default_cell(board->affected_cells[i]);

    board->affected_cells_size = 0;
}

static void
board_add_affected_cell_att(board_t *board, cell_t *cell) {
    if (cell->piece == NULL) 
        return;

    if (cell->piece->color == board->current_cell->piece->color)
        return;

    board->affected_cells[board->affected_cells_size] = cell;
    ++board->affected_cells_size;
    board_make_red_cell(cell);
}

static void
board_add_affected_cell_ex(board_t *board, cell_t *cell) {
    if (cell->piece != NULL) 
        return;

    board->affected_cells[board->affected_cells_size] = cell;
    ++board->affected_cells_size;
    board_make_green_cell(cell);
}

static void 
board_add_affected_cell(board_t *board, cell_t *cell) { 
    if (cell->piece != NULL) {
        if (cell->piece->color != board->current_cell->piece->color) {
            board->affected_cells[board->affected_cells_size] = cell;
            ++board->affected_cells_size;
            board_make_red_cell(cell);
        }
       
        return;
    }

    board->affected_cells[board->affected_cells_size] = cell;
    ++board->affected_cells_size;
    board_make_green_cell(cell);
}

static void
board_render_pawn_moves(board_t *board) {
    cell_t *current = board->current_cell;

    if (current->piece->color == PIECE_COLOR_BLACK) {
        if (current->col >= BOARD_SIZE - 1)
            return;

        if (current->row < BOARD_SIZE - 1)
            board_add_affected_cell_att(board, &board->cells[current->row + 1][current->col + 1]);
        if (current->row > 0)
            board_add_affected_cell_att(board, &board->cells[current->row - 1][current->col + 1]);

        board_add_affected_cell_ex(board, &board->cells[current->row][current->col + 1]);

        if (current->col == 1 && board->cells[current->row][current->col + 1].piece == NULL)
            board_add_affected_cell_ex(board, &board->cells[current->row][current->col + 2]);
        
    } else if (current->piece->color == PIECE_COLOR_WHITE) {
        if (current->col <= 0)
            return;

        if (current->row < BOARD_SIZE - 1)
            board_add_affected_cell_att(board, &board->cells[current->row + 1][current->col - 1]);
        if (current->row > 0)
            board_add_affected_cell_att(board, &board->cells[current->row - 1][current->col - 1]);
        
        board_add_affected_cell_ex(board, &board->cells[current->row][current->col - 1]);
        
        if (current->col == 6 && board->cells[current->row][current->col - 1].piece == NULL)
            board_add_affected_cell_ex(board, &board->cells[current->row][current->col - 2]);
    } 
}

static void
board_render_rook_moves(board_t *board) {
    cell_t *current = board->current_cell;

    for (int i = current->row - 1; i >= 0; --i) {
        board_add_affected_cell(board, &board->cells[i][current->col]);

        if (board->cells[i][current->col].piece != NULL)
            break;
    }
    for (int i = current->row + 1; i < BOARD_SIZE; ++i) {
        board_add_affected_cell(board, &board->cells[i][current->col]);

        if (board->cells[i][current->col].piece != NULL)
            break;
    }
    
    for (int i = current->col - 1; i >= 0; --i) {
        board_add_affected_cell(board, &board->cells[current->row][i]);

        if (board->cells[current->row][i].piece != NULL)
            break;
    }
    for (int i = current->col + 1; i < BOARD_SIZE; ++i) {
        board_add_affected_cell(board, &board->cells[current->row][i]);

        if (board->cells[current->row][i].piece != NULL)
            break;
    }
}

static void
board_render_knight_moves(board_t *board) {
    cell_t *current = board->current_cell;
    
    if (current->row < BOARD_SIZE - 1&& current->col < BOARD_SIZE - 2)
        board_add_affected_cell(board, &board->cells[current->row + 1][current->col + 2]);
    if (current->row > 0 && current->col < BOARD_SIZE - 2)
        board_add_affected_cell(board, &board->cells[current->row - 1][current->col + 2]);
    if (current->row < BOARD_SIZE - 1 && current->col > 1)
        board_add_affected_cell(board, &board->cells[current->row + 1][current->col - 2]);
    if (current->row > 0 && current->col > 1)
        board_add_affected_cell(board, &board->cells[current->row - 1][current->col - 2]);
    if (current->row < BOARD_SIZE - 2 && current->col < BOARD_SIZE - 1)
        board_add_affected_cell(board, &board->cells[current->row + 2][current->col + 1]);
    if (current->row < BOARD_SIZE - 2 && current->col > 0)
        board_add_affected_cell(board, &board->cells[current->row + 2][current->col - 1]);
    if (current->row > 1 && current->col < BOARD_SIZE - 1)
        board_add_affected_cell(board, &board->cells[current->row - 2][current->col + 1]);
    if (current->row > 1 && current->col > 0)
        board_add_affected_cell(board, &board->cells[current->row - 2][current->col - 1]);
}

static void 
board_render_bishop_moves(board_t *board) {
    cell_t *current = board->current_cell;

    unsigned char r = current->row;
    unsigned char c = current->col; 
    while (c > 0 && r < BOARD_SIZE - 1) {
        ++r;
        --c;
        board_add_affected_cell(board, &board->cells[r][c]);
        
        if (board->cells[r][c].piece != NULL)
            break;
    }

    r = current->row;
    c = current->col;
    while (c > 0 && r > 0) {
        --r;
        --c;
        board_add_affected_cell(board, &board->cells[r][c]);
        
        if (board->cells[r][c].piece != NULL)
            break;
    }
    
    r = current->row;
    c = current->col;
    while (c < BOARD_SIZE - 1 && r < BOARD_SIZE - 1) {
        ++r;
        ++c;
        board_add_affected_cell(board, &board->cells[r][c]);
        
        if (board->cells[r][c].piece != NULL)
            break;
    }
    
    r = current->row;
    c = current->col;
    while (c < BOARD_SIZE - 1 && r > 0) {
        --r;
        ++c;
        board_add_affected_cell(board, &board->cells[r][c]);
        
        if (board->cells[r][c].piece != NULL)
            break;
    }
}

static void
board_render_queen_moves(board_t *board) {
    board_render_rook_moves(board);
    board_render_bishop_moves(board);
}

static void
board_render_king_moves(board_t *board) {
    cell_t *current = board->current_cell;

    if (current->row < BOARD_SIZE - 1)
        board_add_affected_cell(board, &board->cells[current->row + 1][current->col]);
    if (current->row > 0)
        board_add_affected_cell(board, &board->cells[current->row - 1][current->col]);
    if (current->col < BOARD_SIZE - 1)
        board_add_affected_cell(board, &board->cells[current->row][current->col + 1]);
    if (current->col > 0)
        board_add_affected_cell(board, &board->cells[current->row][current->col - 1]);
    
    if (current->row < BOARD_SIZE - 1 && current->col < BOARD_SIZE - 1)
        board_add_affected_cell(board, &board->cells[current->row + 1][current->col + 1]);
    if (current->row > 0 && current->col > 0)
        board_add_affected_cell(board, &board->cells[current->row - 1][current->col - 1]);
    if (current->row < BOARD_SIZE - 1 && current->col > 0)
        board_add_affected_cell(board, &board->cells[current->row + 1][current->col - 1]);
    if (current->row > 0 && current->col < BOARD_SIZE - 1)
        board_add_affected_cell(board, &board->cells[current->row - 1][current->col + 1]);
}

static void
board_draw_piece_moves(board_t *board) {
    cell_t *current = board->current_cell;
    
    switch(current->piece->type) {
        case PIECE_TYPE_PAWN:
            board_render_pawn_moves(board);
            break;
        case PIECE_TYPE_ROOK:
            board_render_rook_moves(board);
            break;
        case PIECE_TYPE_KNIGHT:
            board_render_knight_moves(board);
            break;
        case PIECE_TYPE_BISHOP:
            board_render_bishop_moves(board);
            break;
        case PIECE_TYPE_QUEEN:
            board_render_queen_moves(board);
            break;
        case PIECE_TYPE_KING:
            board_render_king_moves(board);
            break;
    }
}

void
board_set_current_cell(board_t *board, int col, int row) {
    if (board->cells[col][row].piece == NULL)
        return;
    
    board_clear_affected_cells(board);

    if (board->current_cell != NULL) 
        board_make_default_cell(board->current_cell);

    board->current_cell = &board->cells[col][row];

    board_make_blue_cell(board->current_cell);
    board_draw_piece_moves(board);
}

void
board_move_to_cell(board_t *board, int col, int row) {
    if (board->affected_cells <= 0)
        return;

    for (int i = 0; i < board->affected_cells_size; ++i) {
        if (board->affected_cells[i]->col == row && board->affected_cells[i]->row == col) {
            board->affected_cells[i]->piece = board->current_cell->piece;
            board->current_cell->piece = NULL;
            
            board_make_default_cell(board->current_cell);
            board_clear_affected_cells(board);
            return;
        }
    }
}


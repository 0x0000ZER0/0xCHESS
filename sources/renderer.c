#include "renderer.h"
#include "error.h"

renderer_t*
renderer_create(window_t *win) {
    renderer_t *renderer = SDL_CreateRenderer(win, -1, SDL_RENDERER_ACCELERATED);

    if (renderer == NULL)
        error_print(AT, "The renderer is NULL");

    return renderer;
}

void 
renderer_destroy(renderer_t *r) {
    SDL_DestroyRenderer(r);
}

void 
renderer_clear(renderer_t *r) {
    SDL_RenderClear(r);
}

void 
renderer_set_color(renderer_t *r, rgba32_t *color) {
    SDL_SetRenderDrawColor(r, color->red, color->green, color->blue, color->alpha);
}

void 
renderer_update(renderer_t *r) {
    SDL_RenderPresent(r);
}

void 
renderer_draw_rect(renderer_t *ren, rect_t *rec) {
    SDL_RenderFillRect(ren, rec);
}

void 
renderer_draw_texture(renderer_t *ren, texture_t *tex, rect_t *rec) {
    if (SDL_RenderCopy(ren, tex, NULL, rec) < 0) 
        error_print(AT, "Can NOT render the texture");
}

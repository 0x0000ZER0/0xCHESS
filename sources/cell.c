#include "cell.h"
#include "piece.h"
#include "rect.h"
#include "board.h"
#include "renderer.h"
#include "rgba32.h"

void 
cell_render(cell_t *cell, renderer_t *renderer) {
    rect_t rect = { 
        cell->row * CELL_SIZE, 
        cell->col * CELL_SIZE,  
        CELL_SIZE,
        CELL_SIZE
    };

    renderer_set_color(renderer, &cell->color);
    renderer_draw_rect(renderer, &rect);
}


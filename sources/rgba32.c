#include "rgba32.h"

char
rgba32_is_equal(rgba32_t* color1, rgba32_t* color2) {
    return color1->red   == color2->red   &&
           color1->green == color2->green &&
           color1->blue  == color2->blue  &&
           color1->alpha == color2->alpha;
}

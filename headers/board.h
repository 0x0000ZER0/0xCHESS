#ifndef BOARD_H
#define BOARD_H

#define BOARD_SIZE 8
#define BOARD_MAX_MOVE_CELLS 21

#include "cell.h"

typedef struct {
    cell_t cells[BOARD_SIZE][BOARD_SIZE];
    cell_t *current_cell;
    cell_t *affected_cells[BOARD_MAX_MOVE_CELLS];
    unsigned int affected_cells_size;
} board_t;

void 
board_create(renderer_t*, board_t*);

void
board_destroy(board_t*);

void 
board_render(board_t*, renderer_t*);

void
board_set_current_cell(board_t*, int, int);

void
board_move_to_cell(board_t*, int, int);

#endif

#ifndef RENDERER_H
#define RENDERER_H

#include "window.h"
#include "rgba32.h"
#include "rect.h"

typedef SDL_Texture texture_t;
typedef SDL_Renderer renderer_t;

renderer_t*
renderer_create(window_t*);

void 
renderer_destroy(renderer_t*);

void 
renderer_clear(renderer_t*);

void 
renderer_set_color(renderer_t*, rgba32_t*);

void 
renderer_update(renderer_t*);

void 
renderer_draw_rect(renderer_t*, rect_t*);

void 
renderer_draw_texture(renderer_t*, texture_t*, rect_t*);

#endif

#ifndef GAME_H
#define GAME_H

#include <SDL2/SDL_image.h>

#include "renderer.h"
#include "event.h"
#include "board.h"

typedef struct {
    window_t *window;
    renderer_t *renderer;
    board_t board;
} game_t;

game_t*
game_create();

void 
game_destroy(game_t*);

#endif

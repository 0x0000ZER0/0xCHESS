#ifndef SURFACE_H
#define SURFACE_H

#include <SDL2/SDL.h>

typedef SDL_Surface surface_t;

surface_t*
surface_load(const char*);

void 
surface_destroy(surface_t*);

#endif

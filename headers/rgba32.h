#ifndef RGBA32_H
#define RGBA32_H

#define RGBA_DEAULT { 0,   0,   0,   255 };

#define RGBA_BLACK  { 0,   0,   0,   255 };
#define RGBA_WHITE  { 255, 255, 255, 255 };
#define RGBA_RED    { 255, 0,   0,   255 };
#define RGBA_GREEN  { 0,   255, 0,   255 };
#define RGBA_BLUE   { 0,   0,   255, 255 };

typedef struct {
    unsigned char red;
    unsigned char green;
    unsigned char blue;
    unsigned char alpha;
} rgba32_t;

char
rgba32_is_equal(rgba32_t*, rgba32_t*);

#endif

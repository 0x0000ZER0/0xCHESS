#ifndef ERROR_H
#define ERROR_H

#define WINDOW_IS_NULL   -1
#define RENDERER_IS_NULL -2
#define SURFACE_IS_NULL  -3
#define TEXTURE_IS_NULL  -4

#define STRINGIFY(x) #x
#define TOSTRING(x) STRINGIFY(x)
#define AT __FILE__ ":" TOSTRING(__LINE__)

void
error_print(const char*, const char*);

#endif

#ifndef TEXTURE_H
#define TEXTURE_H

#include <SDL2/SDL.h>

#include "renderer.h"
#include "rgba32.h"
#include "surface.h"
#include "rect.h"

typedef SDL_Texture texture_t;

texture_t*
texture_load(renderer_t*, const char*);

void 
texture_destroy(texture_t*);

void
texture_set_color(texture_t*, rgba32_t*);

texture_t*
texture_create_from_surface(renderer_t*, surface_t*);

#endif

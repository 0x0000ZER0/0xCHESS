#ifndef MOUSE_H
#define MOUSE_H

#include <SDL2/SDL.h>

typedef SDL_MouseButtonEvent mouse_event;

void
mouse_get_state(int*, int*);

#endif

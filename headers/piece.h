#ifndef PIECE_H
#define PIECE_H

#define PIECE_COLOR_NONE  -1
#define PIECE_COLOR_BLACK  0
#define PIECE_COLOR_WHITE  1

#define PIECE_TYPE_NONE   -1
#define PIECE_TYPE_PAWN    0
#define PIECE_TYPE_ROOK    1
#define PIECE_TYPE_KNIGHT  2
#define PIECE_TYPE_BISHOP  3
#define PIECE_TYPE_QUEEN   4
#define PIECE_TYPE_KING    5

#include "renderer.h"

typedef struct {
    char type;
    char color;
    texture_t *texture;
} piece_t;

piece_t*
piece_create();

piece_t*
piece_create_pawn(renderer_t*, char);

piece_t*
piece_create_rook(renderer_t*, char);

piece_t*
piece_create_knight(renderer_t*, char);

piece_t*
piece_create_bishop(renderer_t*, char);

piece_t*
piece_create_queen(renderer_t*, char);

piece_t*
piece_create_king(renderer_t*, char);

void
piece_destroy(piece_t*);

void 
piece_render(piece_t*, renderer_t*, rect_t*);

#endif

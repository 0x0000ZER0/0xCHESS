#ifndef WINDOW_PARAMS_H
#define WINDOW_PARAMS_H

#define WINDOW_PARAMS_DEFAULT {\
    "",\
    SDL_WINDOWPOS_UNDEFINED,\
    SDL_WINDOWPOS_UNDEFINED,\
    0,\
    0\
};

typedef struct {
    const char *title;
    int x;
    int y;
    int w;
    int h;

} window_params_t;

#endif

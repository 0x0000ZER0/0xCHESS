#ifndef CELL_H
#define CELL_H

#define CELL_SIZE 100

#include "rgba32.h"
#include "renderer.h"
#include "piece.h"

typedef struct {
    unsigned short row;
    unsigned short col;
    rgba32_t color;
    piece_t *piece;
} cell_t;

void 
cell_render(cell_t*, renderer_t*);

#endif

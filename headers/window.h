#ifndef WINDOW_H
#define WINDOW_H

#include <SDL2/SDL.h>

#include "window_params.h"

typedef SDL_Window window_t;

window_t*
window_create(window_params_t*);

void 
window_destroy(window_t*);

#endif

#ifndef EVENT_H
#define EVENT_H

#include <SDL2/SDL.h>

typedef SDL_Event event_t;

int 
event_poll(event_t*);

#endif
